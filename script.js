const video = document.querySelector("#video-player")
const playButton = document.querySelector("#play")
const audioController = document.querySelector("#audio-controller")
const current = document.querySelector("#current-time")
// Initializing the volume slider so that is display the correct volume value at the start
audioController.value = video.volume * 100
// For playing/pausing video
function TogglePlay() {
    if (video.paused || video.ended) {
        video.play();
    } else {
        video.pause();
    }
}
// Changing video volume
function SetVolume(value)
{
    video.volume = value / 100;
}
// Changing icon of play/pause button
function UpdateToggleButton() {
    playButton.innerHTML = video.paused ? "►" : "❚❚";
}
// Updating current time that is displayed on the video
function UpdateTime () {
    current.innerHTML = new Date(video.currentTime * 1000).toISOString().substring(14, 19);
}

playButton.addEventListener("click", TogglePlay);
video.addEventListener("click", TogglePlay);
video.addEventListener("play", UpdateToggleButton);
video.addEventListener("pause", UpdateToggleButton);
video.addEventListener("timeupdate", UpdateTime);
